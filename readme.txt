﻿=== Plugin Name ===
Plugin Name: Bets
Description: Создание типа записи "Букмекер" и его описания
Version: 2.0.7
Author: http://andrij200390.96.lt
License: GPLv2 or later
Text Domain: Helper
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

Создание типа записи Букмекер. Для него будет возможность писать рейтинг, добавлять ссылку, валюту, описание, и другое. Имеет свой вывод, заточенный под тему consult press. В плагине имеется возможность вывода топ 3 букмекеров, для етого нужно вставить шорткод [topbets], или вставить виджет ТОП5 букмекеров в нужный сайдбар. Для вывода всех букмекеров используется шорткода [allbets]. Также есть возможность вставки в виде табов (последние и топ букмекеры). Вывод описания тоже сделан через табы. Также добавлен визуальный редактор для описания букмекеров. Добавлены метки для типов записи букмекер. Bitbucket будет синхронизация версии плагина

== Changelog ==

1.0 - Начальный релиз
